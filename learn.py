# -*- coding: utf-8 -*-
import os
import sys
import csv
import random
import datetime
import numpy as np
import matplotlib.pyplot as plt
from keras import regularizers
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, Model
from keras.optimizers import SGD
from keras.layers import Input, Dropout, Flatten, Conv2D, MaxPooling2D, GlobalAveragePooling2D, Dense, Activation, BatchNormalization
from keras.callbacks import Callback, ModelCheckpoint, EarlyStopping, ReduceLROnPlateau, CSVLogger
from keras.applications.inception_v3 import InceptionV3

# ------------------ #
# global env         #
# ------------------ #
HOME             = '/home/ubuntu/cookpad'
MODELS           = os.path.join(HOME, 'models')
LOGS             = os.path.join(HOME, 'logs')
DATASET          = os.path.join(HOME, 'dataset')
TEST_ALL         = os.path.join(DATASET, 'testall')
TRAIN            = [os.path.join(DATASET, 'train1'),
                    os.path.join(DATASET, 'train2'),
                    os.path.join(DATASET, 'train3'),
                    os.path.join(DATASET, 'train4'),
                    os.path.join(DATASET, 'train5'),
                    os.path.join(DATASET, 'train6'),
                    os.path.join(DATASET, 'train7')]
VALIDATION       = [os.path.join(DATASET, 'validation1'),
                    os.path.join(DATASET, 'validation2'),
                    os.path.join(DATASET, 'validation3'),
                    os.path.join(DATASET, 'validation4'),
                    os.path.join(DATASET, 'validation5'),
                    os.path.join(DATASET, 'validation6'),
                    os.path.join(DATASET, 'validation7')]
TRAIN_1          = os.path.join(DATASET, 'train_1')
TRAIN_2          = os.path.join(DATASET, 'train_2')
TRAIN_3          = os.path.join(DATASET, 'train_3')
MASTER           = os.path.join(DATASET, 'master.tsv')
TRAIN_MASTER     = os.path.join(DATASET, 'train_master.tsv')
VALIDATION_NUM   = 30
K_FOLD_NUM       = 7 
RESET_VALIDATION = False
ROWS             = 299
COLS             = 299
NUM_CLASSES      = 55
BATCH_SIZE       = 64
OPTIMIZER        = 'adam'
STEPS_PER_EPOCH  = 300
VALIDATION_STEPS = 300
EPOCHS           = 100


def _read_tsv(tsv_file):
    tsv = []
    for row in open(tsv_file, 'r'):
        tsv.append(row[:-1].split('\t'))
    return tsv


def _count_files(path):
    count = 0
    if os.path.isdir(path):
        count = len(os.listdir(path))
    return count


def generate(train_path, validation_path, batch_size, rows, cols):
    train_datagen = ImageDataGenerator(
        rescale=1.0 / 255,
        zoom_range=0.5,
        horizontal_flip=True,
        vertical_flip=True)

    test_datagen = ImageDataGenerator(rescale=1.0 / 255)

    train_generator = train_datagen.flow_from_directory(
        train_path,
        target_size=(rows, cols),
        batch_size=batch_size,
        class_mode='categorical')

    test_generator = test_datagen.flow_from_directory(
        validation_path,
        target_size=(rows, cols),
        batch_size=batch_size,
        class_mode='categorical')

    print(train_generator.class_indices)

    return train_generator, test_generator


def inceptionv3(rows, cols, num_classes):
    base_model = InceptionV3(
    include_top=False,
    weights=None,
    input_tensor=None,
    input_shape=(rows, cols, 3),
    pooling=None)
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    x = Dropout(0.5)(x)
    predictions = Dense(num_classes,
                  kernel_regularizer=regularizers.l2(0.01),
                  activity_regularizer=regularizers.l1(0.01),
                  activation='softmax')(x)
    model = Model(inputs=base_model.input, outputs=predictions)
    return model


def model_checkpoint(filepath, kfold):
    d = datetime.datetime.today().strftime("%Y-%m-%d")
    callback = ModelCheckpoint(
    filepath=os.path.join(filepath, 'model.{0}fold.{1}.hdf5'.format(kfold, d)),
    monitor='val_loss',
    verbose=1,
    save_best_only=True,
    mode='auto',
    save_weights_only=False,
    period=1)
    return callback


def early_stopping():
    callback = EarlyStopping(
    monitor='val_loss',
    min_delta=0,
    patience=20,
    verbose=0,
    mode='max')
    return callback


def reduce_lr_on_plateau():
    callback = ReduceLROnPlateau(
    monitor='val_loss',
    factor=0.1,
    patience=10,
    verbose=0,
    mode='auto',
    epsilon=0.0001,
    cooldown=0,
    min_lr=0.001)
    return callback


def csv_logger(filename):
    callback = CSVLogger(
    filename,
    separator=',',
    append=False)
    return callback

if __name__ == '__main__':
    for fold in range(1, K_FOLD_NUM):
        model = inceptionv3(ROWS, COLS, NUM_CLASSES)
        model.compile(optimizer=OPTIMIZER, loss='categorical_crossentropy', metrics=['accuracy'])
        model.summary()

        mc_cb = model_checkpoint(MODELS, fold)
        rp_cb = reduce_lr_on_plateau()
        es_cb = early_stopping()
        cl_cb = csv_logger(os.path.join(LOGS, 'csvlogger.{0}fold.csv'.format(fold)))

        train_generator, test_generator = generate(TRAIN[fold - 1], VALIDATION[fold - 1], BATCH_SIZE, ROWS, COLS)
        history = model.fit_generator(
        train_generator,
        epochs=EPOCHS,
        steps_per_epoch=STEPS_PER_EPOCH,
        validation_data=test_generator,
        validation_steps=VALIDATION_STEPS,
        callbacks=[mc_cb, es_cb, rp_cb, cl_cb])
