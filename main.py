# -*- coding: utf-8 -*-
import os
import sys
import csv
import random
import datetime
import numpy as np
import matplotlib.pyplot as plt
from keras import regularizers
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, Model
from keras.optimizers import SGD
from keras.layers import Input, Dropout, Flatten, Conv2D, MaxPooling2D, GlobalAveragePooling2D, Dense, Activation, BatchNormalization
from keras.callbacks import Callback, ModelCheckpoint, ReduceLROnPlateau, CSVLogger
from keras.applications.inception_v3 import InceptionV3

# ------------------ #
# global env         #
# ------------------ #
HOME             = '/home/ubuntu/cookpad'
MODELS           = os.path.join(HOME, 'models')
LOGS             = os.path.join(HOME, 'logs')
DATASET          = os.path.join(HOME, 'dataset')
TEST             = os.path.join(DATASET, 'test')
TRAIN            = os.path.join(DATASET, 'train')
VALIDATION       = os.path.join(DATASET, 'validation')
TRAIN_1          = os.path.join(DATASET, 'train_1')
TRAIN_2          = os.path.join(DATASET, 'train_2')
TRAIN_3          = os.path.join(DATASET, 'train_3')
MASTER           = os.path.join(DATASET, 'master.tsv')
TRAIN_MASTER     = os.path.join(DATASET, 'train_master.tsv')
VALIDATION_NUM   = 20 
RESET_VALIDATION = False
ROWS             = 299
COLS             = 299
NUM_CLASSES      = 55
BATCH_SIZE       = 64
OPTIMIZER        = 'adam'
STEPS_PER_EPOCH  = 300
VALIDATION_STEPS = 300
EPOCHS           = 150


def init():
    if not os.path.exists(TRAIN):
        os.system('mkdir {0}'.format(TRAIN))
        _create_category_directory(TRAIN)

    if not os.path.exists(VALIDATION):
        os.system('mkdir {0}'.format(VALIDATION))
        _create_category_directory(VALIDATION)


def _create_category_directory(path):
    master_tsv = _read_tsv(MASTER)
    for row in master_tsv:
        master_category_name = row[0]
        master_category_id   = row[1]
        os.system('mkdir {0}'.format(os.path.join(path, master_category_name)))


# Move files path/ to train/
def mv_file(path):
    source_path = path 
    target_path = TRAIN
    print('moving files...')
    print('source_path:{0}'.format(source_path))
    print('target_path:{0}'.format(target_path))
    master_tsv       = _read_tsv(MASTER)
    train_master_tsv = _read_tsv(TRAIN_MASTER)
    for row in train_master_tsv:
        file_name   = row[0]
        category_id = row[1]
        source_file = os.path.join(source_path, file_name)
        if os.path.exists(source_file):
            for r in master_tsv:
                master_category_name = r[0]
                master_category_id   = r[1]
                if category_id == master_category_id:
                    target_dir = os.path.join(target_path, master_category_name)
                    os.system('mv {0} {1}'.format(source_file, target_dir))
    os.system('rmdir {0}'.format(source_path))
    print('Done.')


# Move file train/ to validation/
def mv_validation():
    source_path = TRAIN
    target_path = VALIDATION
    print('moving validation files...')
    print('source_path:{0}'.format(source_path))
    print('target_path:{0}'.format(target_path))
    for category_name in os.listdir(source_path):
        source_dir = os.path.join(source_path, category_name)
        target_dir = os.path.join(target_path, category_name)
        validation_files = random.sample(os.listdir(source_dir), VALIDATION_NUM)
        for validation_file in validation_files:
            source_file = os.path.join(source_dir, validation_file) 
            os.system('mv {0} {1}'.format(source_file, target_dir))
    print('Done.')


def rm_category(path):
    print('removing {0} category directory.'.format(path))
    target_dir = path
    for category in os.listdir(path):
        source_dir = os.path.join(path, category)
        if os.path.isdir(source_dir):
            for file_name in os.listdir(source_dir):
                source_file = os.path.join(source_dir, file_name)
                os.system('mv {0} {1}'.format(source_file, target_dir))    
            if _count_files(source_dir) == 0:
                os.system('rmdir {0}'.format(source_dir))
            else:
                print('Cannot remove {0}!'.format(source_dir))
    print('Done.')


def _read_tsv(tsv_file):
    tsv = []
    for row in open(tsv_file, 'r'):
        tsv.append(row[:-1].split('\t'))
    return tsv


def _count_files(path):
    count = 0
    if os.path.isdir(path):
        count = len(os.listdir(path))
    return count


def preprocess(train_path, validation_path, batch_size, rows, cols):
    train_datagen = ImageDataGenerator(
        rescale=1.0 / 255,
        zoom_range=0.5,
        horizontal_flip=True,
        vertical_flip=True)

    test_datagen = ImageDataGenerator(rescale=1.0 / 255)

    train_generator = train_datagen.flow_from_directory(
        train_path,
        target_size=(rows, cols),
        batch_size=batch_size,
        class_mode='categorical')

    test_generator = test_datagen.flow_from_directory(
        validation_path,
        target_size=(rows, cols),
        batch_size=batch_size,
        class_mode='categorical')

    print(train_generator.class_indices)

    return train_generator, test_generator

def AlexNet(rows, cols, num_classes):
    model = Sequential()
 
    model.add(Conv2D(48, 11, strides=3, activation='relu', padding='same', input_shape=(rows, cols, 3)))
    model.add(MaxPooling2D(3, strides=2))
    model.add(BatchNormalization())
 
    model.add(Conv2D(128, 5, strides=3, activation='relu', padding='same'))
    model.add(MaxPooling2D(3, strides=2))
    model.add(BatchNormalization())
 
    model.add(Conv2D(192, 3, strides=1, activation='relu', padding='same'))
    model.add(Conv2D(192, 3, strides=1, activation='relu', padding='same'))
    model.add(Conv2D(128, 3, strides=1, activation='relu', padding='same'))
    model.add(MaxPooling2D(3, strides=2))
    model.add(BatchNormalization())
 
    model.add(Flatten())
    model.add(Dense(2048, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2048, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation='softmax'))
    return model


def inceptionv3(rows, cols, num_classes):
    base_model = InceptionV3(
    include_top=False,
    weights=None,
    input_tensor=None,
    input_shape=(rows, cols, 3),
    pooling=None)
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    x = Dropout(0.5)(x)
    predictions = Dense(num_classes,
                  kernel_regularizer=regularizers.l2(0.01),
                  activity_regularizer=regularizers.l1(0.01),
                  activation='softmax')(x)
    model = Model(inputs=base_model.input, outputs=predictions)
    return model


def model_checkpoint(filepath):
    d = datetime.datetime.today().strftime("%Y-%m-%d")
    callback = ModelCheckpoint(
    filepath=os.path.join(filepath, 'model.{0}.hdf5'.format(d)),
    monitor='val_loss',
    verbose=1,
    save_best_only=True,
    mode='auto',
    save_weights_only=False,
    period=1)
    return callback

def reduce_lr_on_plateau():
    callback = ReduceLROnPlateau(
    monitor='val_loss',
    factor=0.1,
    patience=10,
    verbose=0,
    mode='auto',
    epsilon=0.0001,
    cooldown=0,
    min_lr=0.001)
    return callback


def csv_logger(filename):
    callback = CSVLogger(
    filename,
    separator=',',
    append=False)
    return callback

if __name__ == '__main__':
    init()
    if _count_files(os.path.join(TRAIN, 'abokado')) == 0:
        print('We dont move train files yet.')
        mv_file(TRAIN_1)
        mv_file(TRAIN_2)
        mv_file(TRAIN_3)
    if _count_files(os.path.join(VALIDATION, 'abokado')) == 0:
        print('We dont move validation files yet.')
        mv_validation()

    if RESET_VALIDATION:
        rm_category(VALIDATION)
        mv_file(VALIDATION)

    train_generator, test_generator = preprocess(TRAIN, VALIDATION, BATCH_SIZE, ROWS, COLS)

    model = inceptionv3(ROWS, COLS, NUM_CLASSES)
    model.compile(optimizer=OPTIMIZER, loss='categorical_crossentropy', metrics=['accuracy'])
    model.summary()

    mc_cb = model_checkpoint(MODELS)
    rp_cb = reduce_lr_on_plateau()
    cl_cb = csv_logger(os.path.join(LOGS, 'csvlogger.csv'))

    history = model.fit_generator(
    train_generator,
    epochs=EPOCHS,
    steps_per_epoch=STEPS_PER_EPOCH,
    validation_data=test_generator,
    validation_steps=VALIDATION_STEPS,
    callbacks=[mc_cb, rp_cb, cl_cb])

#    score = model.evaluate_generator(test_generator)
#    print('Test loss:', score[0])
#    print('Test accuracy:', score[1])

#    print('save model.')
#    model.save('alex.hdf5')
