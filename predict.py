import os
import numpy as np
from keras.preprocessing import image
from keras.models import load_model

HOME       = '/home/ubuntu/cookpad'
MODEL      = os.path.join(HOME, 'models/model.2018-03-17.hdf5')
DATASET    = os.path.join(HOME, 'dataset')
TEST       = os.path.join(DATASET, 'test')
SUBMIT     = os.path.join(DATASET, 'submit.tsv')
ROWS       = 299
COLS       = 299
OPTIMIZER  = 'adam'
BATCH_SIZE = 1

def loadmodel(model, optimizer):
    model = load_model(model)
#    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
    model.summary()
    return model


if __name__ == '__main__':
    model = loadmodel(MODEL, OPTIMIZER)

    result = []
    filenames = os.listdir(TEST)
#    for filename in filenames:
#        img = image.load_img(os.path.join(TEST, filename), target_size=(ROWS, COLS))
#        x = image.img_to_array(img)
#        x = np.expand_dims(x,0)
#        images.append(x)
    print('predicting test data.')
    os.system('rm {0}'.format(SUBMIT))
    for filename in filenames:
        print('predicting {0}'.format(filename))
        img = image.load_img(os.path.join(TEST, filename), target_size=(ROWS, COLS))
        x = image.img_to_array(img) / 255
        x = np.expand_dims(x,0)
        x = np.argmax(model.predict(x))
        os.system("echo -e '{0}\t{1}' >> {2}".format(filename, x, SUBMIT))

    os.system('mv {0} tmp.tsv'.format(SUBMIT))
    os.system('sort -k 2 -t "_" -n tmp.tsv | sed -e "s/-e //g" > {0}'.format(SUBMIT))
    os.system('rm tmp.tsv')
    print('Done.')
