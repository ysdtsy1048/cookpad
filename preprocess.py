# -*- coding: utf-8 -*-
import os
import sys
import csv
import random
import datetime
import numpy as np
import matplotlib.pyplot as plt
from keras import regularizers
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, Model
from keras.optimizers import SGD
from keras.layers import Input, Dropout, Flatten, Conv2D, MaxPooling2D, GlobalAveragePooling2D, Dense, Activation, BatchNormalization
from keras.callbacks import Callback, ModelCheckpoint, ReduceLROnPlateau, CSVLogger
from keras.applications.inception_v3 import InceptionV3

# ------------------ #
# global env         #
# ------------------ #
HOME             = '/home/ubuntu/cookpad'
MODELS           = os.path.join(HOME, 'models')
LOGS             = os.path.join(HOME, 'logs')
DATASET          = os.path.join(HOME, 'dataset')
TEST_ALL         = os.path.join(DATASET, 'testall')
TRAIN            = [os.path.join(DATASET, 'train1'),
                    os.path.join(DATASET, 'train2'),
                    os.path.join(DATASET, 'train3'),
                    os.path.join(DATASET, 'train4'),
                    os.path.join(DATASET, 'train5'),
                    os.path.join(DATASET, 'train6'),
                    os.path.join(DATASET, 'train7')]
VALIDATION       = [os.path.join(DATASET, 'validation1'),
                    os.path.join(DATASET, 'validation2'),
                    os.path.join(DATASET, 'validation3'),
                    os.path.join(DATASET, 'validation4'),
                    os.path.join(DATASET, 'validation5'),
                    os.path.join(DATASET, 'validation6'),
                    os.path.join(DATASET, 'validation7')]
TRAIN_1          = os.path.join(DATASET, 'train_1')
TRAIN_2          = os.path.join(DATASET, 'train_2')
TRAIN_3          = os.path.join(DATASET, 'train_3')
MASTER           = os.path.join(DATASET, 'master.tsv')
TRAIN_MASTER     = os.path.join(DATASET, 'train_master.tsv')
VALIDATION_NUM   = 30
K_FOLD_NUM       = 7 
RESET_VALIDATION = False
ROWS             = 299
COLS             = 299
NUM_CLASSES      = 55
BATCH_SIZE       = 64
OPTIMIZER        = 'adam'
STEPS_PER_EPOCH  = 300
VALIDATION_STEPS = 300
EPOCHS           = 100


def init(train, validation):
    if not os.path.exists(train):
        os.system('mkdir {0}'.format(train))
        _create_category_directory(train)

    if not os.path.exists(validation):
        os.system('mkdir {0}'.format(validation))
        _create_category_directory(validation)


def _create_category_directory(path):
    master_tsv = _read_tsv(MASTER)
    for row in master_tsv:
        master_category_name = row[0]
        master_category_id   = row[1]
        os.system('mkdir {0}'.format(os.path.join(path, master_category_name)))


# Move files path/ to train/
def cp_file(path, train):
    source_path = path 
    target_path = train
    print('copying files...')
    print('source_path:{0}'.format(source_path))
    print('target_path:{0}'.format(target_path))
    master_tsv       = _read_tsv(MASTER)
    train_master_tsv = _read_tsv(TRAIN_MASTER)
    for row in train_master_tsv:
        file_name   = row[0]
        category_id = row[1]
        source_file = os.path.join(source_path, file_name)
        if os.path.exists(source_file):
            for r in master_tsv:
                master_category_name = r[0]
                master_category_id   = r[1]
                if category_id == master_category_id:
                    target_dir = os.path.join(target_path, master_category_name)
                    os.system('cp {0} {1}'.format(source_file, target_dir))
#   os.system('rmdir {0}'.format(source_path))
    print('Done.')


# Move file train/ to validation/
def mv_validation(train, validation, n):
    source_path = train
    target_path = validation
    print('moving validation files...')
    print('source_path:{0}'.format(source_path))
    print('target_path:{0}'.format(target_path))
    for category_name in os.listdir(source_path):
        source_dir = os.path.join(source_path, category_name)
        target_dir = os.path.join(target_path, category_name)
        if n == K_FOLD_NUM:
            validation_files = os.listdir(source_dir)[n*VALIDATION_NUM:]
        else:
            validation_files = os.listdir(source_dir)[n*VALIDATION_NUM:n*VALIDATION_NUM+VALIDATION_NUM]
        for validation_file in validation_files:
            source_file = os.path.join(source_dir, validation_file) 
            os.system('mv {0} {1}'.format(source_file, target_dir))
    print('Done.')


def rm_category(path):
    print('removing {0} category directory.'.format(path))
    target_dir = path
    for category in os.listdir(path):
        source_dir = os.path.join(path, category)
        if os.path.isdir(source_dir):
            for file_name in os.listdir(source_dir):
                source_file = os.path.join(source_dir, file_name)
                os.system('mv {0} {1}'.format(source_file, target_dir))    
            if _count_files(source_dir) == 0:
                os.system('rmdir {0}'.format(source_dir))
            else:
                print('Cannot remove {0}!'.format(source_dir))
    print('Done.')


def _read_tsv(tsv_file):
    tsv = []
    for row in open(tsv_file, 'r'):
        tsv.append(row[:-1].split('\t'))
    return tsv


def _count_files(path):
    count = 0
    if os.path.isdir(path):
        count = len(os.listdir(path))
    return count


def csv_logger(filename):
    callback = CSVLogger(
    filename,
    separator=',',
    append=False)
    return callback

if __name__ == '__main__':
    for (train, validation) in zip(TRAIN, VALIDATION):
        init(train, validation)

    k_fold = 0
    for (train, validation) in zip(TRAIN, VALIDATION):
        k_fold += 1
        print('{0}_fold.'.format(k_fold))
        if _count_files(os.path.join(train, 'abokado')) == 0:
            print('We dont cp train files yet.')
            cp_file(TRAIN_1, train)
            cp_file(TRAIN_2, train)
            cp_file(TRAIN_3, train)
        if _count_files(os.path.join(validation, 'abokado')) == 0:
            print('We dont move validation files yet.')
            mv_validation(train, validation, k_fold)

    if RESET_VALIDATION:
        rm_category(VALIDATION)
        mv_file(VALIDATION)

