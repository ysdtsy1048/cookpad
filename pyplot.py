import os
import numpy as np
import matplotlib.pyplot as plt

#LOG_PATH = '/home/ubuntu/cookpad/logs/csvlogger.csv'
LOG_PATH = '/home/ubuntu/cookpad/logs/csvlogger.2fold.csv'
FIG_PATH = '/home/ubuntu/cookpad/logs/csvlogger.png'

d = np.genfromtxt(LOG_PATH, delimiter=',', names=True)
fig, ax = plt.subplots(1)
ax.plot(d['epoch'], d['acc'], c='k')
ax.plot(d['epoch'], d['val_acc'], c='r')
ax.legend(('Train', 'Validation'))
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.grid()
fig.savefig(FIG_PATH)
