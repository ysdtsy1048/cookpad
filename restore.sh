#!/bin/bash

echo 'removing /home/ubuntu/cookpad/dataset/test/'
rm -rf /home/ubuntu/cookpad/dataset/test/
echo 'removing /home/ubuntu/cookpad/dataset/train/'
rm -rf /home/ubuntu/cookpad/dataset/train/
echo 'removing /home/ubuntu/cookpad/dataset/train_1/'
rm -rf /home/ubuntu/cookpad/dataset/train_1/
echo 'removing /home/ubuntu/cookpad/dataset/train_2/'
rm -rf /home/ubuntu/cookpad/dataset/train_2/
echo 'removing /home/ubuntu/cookpad/dataset/train_3/'
rm -rf /home/ubuntu/cookpad/dataset/train_3/
echo 'removing /home/ubuntu/cookpad/dataset/validation/'
rm -rf /home/ubuntu/cookpad/dataset/validation/

echo 'copy /home/ubuntu/cookpad/dataset.bk/train_1/'
cp -r /home/ubuntu/cookpad/dataset.bk/train_1/ /home/ubuntu/cookpad/dataset/
echo 'copy /home/ubuntu/cookpad/dataset.bk/train_2/'
cp -r /home/ubuntu/cookpad/dataset.bk/train_2/ /home/ubuntu/cookpad/dataset/
echo 'copy /home/ubuntu/cookpad/dataset.bk/train_3/'
cp -r /home/ubuntu/cookpad/dataset.bk/train_3/ /home/ubuntu/cookpad/dataset/
